# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-04-26 19:59+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"

#: FlOpEDT/settings/base.py:157
msgid "French"
msgstr ""

#: FlOpEDT/settings/base.py:158
msgid "English"
msgstr ""

#: FlOpEDT/settings/base.py:159
msgid "Spanish"
msgstr ""

#: FlOpEDT/settings/base.py:160
msgid "Arabic"
msgstr ""

#: FlOpEDT/settings/base.py:161
msgid "Basque"
msgstr ""

#: FlOpEDT/settings/base.py:162
msgid "Breton"
msgstr ""

#: FlOpEDT/settings/base.py:163
msgid "Catalan"
msgstr ""

#: FlOpEDT/settings/base.py:164
msgid "Corsican"
msgstr ""

#: FlOpEDT/settings/base.py:165
msgid "Danish"
msgstr ""

#: FlOpEDT/settings/base.py:166
msgid "German"
msgstr ""

#: FlOpEDT/settings/base.py:167
msgid "Dutch"
msgstr ""

#: FlOpEDT/settings/base.py:168
msgid "Greek"
msgstr ""

#: FlOpEDT/settings/base.py:169
msgid "Italian"
msgstr ""

#: FlOpEDT/settings/base.py:170
msgid "Latin"
msgstr ""

#: FlOpEDT/settings/base.py:171
msgid "Norwegian"
msgstr ""

#: FlOpEDT/settings/base.py:172
msgid "Portuguese"
msgstr ""

#: FlOpEDT/settings/base.py:173
msgid "Swedish"
msgstr ""

#: FlOpEDT/settings/base.py:174
msgid "Chinese"
msgstr ""

#: FlOpEDT/settings/base.py:175
msgid "Smurf"
msgstr ""

#: base/forms.py:33
msgid "Your e-mail address :"
msgstr ""

#: base/forms.py:36
msgid "Recipient :"
msgstr ""

#: base/forms.py:38
msgid "Pseudo : 4 characters max"
msgstr ""

#: base/forms.py:40
msgid "Subject of the e-mail :"
msgstr ""

#: base/forms.py:42
msgid "100 characters max"
msgstr ""

#: base/forms.py:43
msgid "Body of the message :"
msgstr ""

#: base/forms.py:50
msgid "This person doesn't exist."
msgstr ""

#: base/forms.py:58
msgid "Ideally"
msgstr ""

#: base/forms.py:64 base/templates/base/show-stype.html:134
msgid "Maximum"
msgstr ""

#: base/models.py:82
msgid "Basic group?"
msgstr ""

#: base/models.py:149
msgid "Half day"
msgstr ""

#: base/models.py:296
msgid "Abbreviation"
msgstr ""

#: base/models.py:344
msgid "graded?"
msgstr ""

#: base/models.py:372
msgid "Suspens?"
msgstr ""

#: base/models.py:404
msgid "Graded?"
msgstr ""

#: base/models.py:728
msgid "Successives?"
msgstr ""

#: base/models.py:729
msgid "On different days"
msgstr ""

#: base/models.py:749
msgid "Full"
msgstr ""

#: base/models.py:751
msgid "Full generation date"
msgstr ""

#: base/models.py:752
msgid "Stabilized"
msgstr ""

#: base/models.py:754
msgid "Partial generation date"
msgstr ""

#: base/templates/base/contact.html:34 base/templates/base/visio.html:38
msgid "Send"
msgstr ""

#: base/templates/base/departments.html:15
msgid "Please choose your department : "
msgstr ""

#: base/templates/base/help.html:34
msgid "Room modification"
msgstr ""

#: base/templates/base/help.html:35
msgid ""
"Right click on the course in question, the available rooms appear. Click on "
"+ to choose a room that is not available for this course. Don't forget to "
"validate any changes."
msgstr ""

#: base/templates/base/help.html:38
msgid "Create an account"
msgstr ""

#: base/templates/base/help.html:39
msgid "Here"
msgstr ""

#: base/templates/base/help.html:42
msgid "Modification of an account"
msgstr ""

#: base/templates/base/help.html:43
msgid "Click on his nickname (his initials) at the top right of the page."
msgstr ""

#: base/templates/base/help.html:45
msgid "Change password"
msgstr ""

#: base/templates/base/help.html:46 base/templates/base/help.html:49
msgid "Click on"
msgstr ""

#: base/templates/base/help.html:46 base/templates/base/help.html:49
msgid "this link"
msgstr ""

#: base/templates/base/help.html:46
msgid "to change your password."
msgstr ""

#: base/templates/base/help.html:48
msgid "Password reminder"
msgstr ""

#: base/templates/base/help.html:49
msgid "for a password reminder, or via 'Login' at the top of the page."
msgstr ""

#: base/templates/base/help.html:51
msgid "Direct link to a particular group"
msgstr ""

#: base/templates/base/help.html:52
msgid "Add \"?promo=INFO1&gp=1A\" at the end of the url, for example "
msgstr ""

#: base/templates/base/help.html:54
msgid "How availability entry work"
msgstr ""

#: base/templates/base/help.html:55
msgid ""
"When generating the schedule for a given week, if no availability is entered "
"for that week, the typical week defines the availability."
msgstr ""

#: base/templates/base/help.html:57
msgid "Some Tutorial Videos"
msgstr ""

#: base/templates/base/module_description.html:35
msgid "Modification of the module description."
msgstr ""

#: base/templates/base/module_description.html:41
msgid "Modify"
msgstr ""

#: base/templates/base/notification.html:29
msgid "I wish to be notified of changes for the"
msgstr ""

#: base/templates/base/notification.html:32
msgid "next weeks."
msgstr ""

#: base/templates/base/notification.html:33
msgid "(0 if no notification, 1 for the next 7 days, etc.)"
msgstr ""

#: base/templates/base/notification.html:36
msgid "Save"
msgstr ""

#: base/templates/base/room_preference.html:14
msgid "Favorite room"
msgstr ""

#: base/templates/base/room_preference.html:15
msgid "(Ranking your favorite rooms, which may contain ties.)"
msgstr ""

#: base/templates/base/show-decale.html:79
msgid "When ?"
msgstr ""

#: base/templates/base/show-decale.html:81
msgid "Cancelled class"
msgstr ""

#: base/templates/base/show-decale.html:82
msgid "Pending classes"
msgstr ""

#: base/templates/base/show-decale.html:83
msgid "Course scheduled on week"
msgstr ""

#: base/templates/base/show-decale.html:89
msgid "Why ?"
msgstr ""

#: base/templates/base/show-decale.html:90
msgid "(Please fill in the date information)"
msgstr ""

#: base/templates/base/show-decale.html:98
msgid " Proposals"
msgstr ""

#: base/templates/base/show-decale.html:104
msgid " How ?"
msgstr ""

#: base/templates/base/show-edt.html:49
msgid "Check election"
msgstr ""

#: base/templates/base/show-edt.html:53
msgid "Elect this copy"
msgstr ""

#: base/templates/base/show-edt.html:57
msgid "Duplicate this copy"
msgstr ""

#: base/templates/base/show-edt.html:61
msgid "Delete this copy"
msgstr ""

#: base/templates/base/show-edt.html:65
msgid "Delete all copies except the 0"
msgstr ""

#: base/templates/base/show-edt.html:68
msgid "Reassign rooms"
msgstr ""

#: base/templates/base/show-stype.html:95
msgid "Weeks"
msgstr ""

#: base/templates/base/show-stype.html:96
msgid "Set default week"
msgstr ""

#: base/templates/base/show-stype.html:97
msgid "from week"
msgstr ""

#: base/templates/base/show-stype.html:98
#: base/templates/base/show-stype.html:101
msgid "year"
msgstr ""

#: base/templates/base/show-stype.html:100
msgid "until week"
msgstr ""

#: base/templates/base/show-stype.html:103
msgid "button_apply"
msgstr ""

#: base/templates/base/show-stype.html:108
msgid "Default week"
msgstr ""

#: base/templates/base/show-stype.html:110
msgid "Update my default week"
msgstr ""

#: base/templates/base/show-stype.html:112
#: base/templates/base/show-stype.html:137
msgid "button_save"
msgstr ""

#: base/templates/base/show-stype.html:121
msgid "My videoconf links"
msgstr ""

#: base/templates/base/show-stype.html:122
msgid "Create a link"
msgstr ""

#: base/templates/base/show-stype.html:124
#: base/templates/base/show-stype.html:147
msgid "button_change"
msgstr ""

#: base/templates/base/show-stype.html:127
msgid "My ideal day"
msgstr ""

#: base/templates/base/show-stype.html:130
msgid ""
"This is the total number of teaching hours I would like to give every day:"
msgstr ""

#: base/templates/base/show-stype.html:132
msgid "Preferred"
msgstr ""

#: base/templates/base/show-stype.html:144
msgid "My favorite rooms"
msgstr ""

#: base/templates/base/show-stype.html:145
msgid "Select my favorite rooms"
msgstr ""

#: base/templates/base/show-stype.html:170
msgid "Teachers"
msgstr ""

#: base/templates/base/show-stype.html:171
#: base/templates/base/show-stype.html:191
msgid "Transformation in : "
msgstr ""

#: base/templates/base/show-stype.html:177
#: base/templates/base/show-stype.html:183
msgid "Types of classes"
msgstr ""

#: base/templates/base/show-stype.html:190 ics/templates/ics/index.html:68
msgid "Rooms"
msgstr ""

#: base/templates/base/statistics.html:105
msgid "Number of days of innocuousness per room"
msgstr ""

#: base/templates/base/statistics.html:106
msgid "Number of classes given per teacher"
msgstr ""

#: base/templates/base/visio.html:31
msgid "Create or modify a link"
msgstr ""

#: base/views.py:1700
msgid "Not authorized. New link then."
msgstr ""

#: base/views.py:1703
msgid "Unknown link. New link then."
msgstr ""

#: base/views.py:1715
msgid "Created"
msgstr ""

#: base/views.py:1715
msgid "Modified"
msgstr ""

#: base/views.py:1718
msgid "Tutor does not exist"
msgstr ""

#: base/weeks.py:34
msgid "Mon."
msgstr ""

#: base/weeks.py:35
msgid "Tue."
msgstr ""

#: base/weeks.py:36
msgid "Wed."
msgstr ""

#: base/weeks.py:37
msgid "Thu."
msgstr ""

#: base/weeks.py:38
msgid "Fri."
msgstr ""

#: base/weeks.py:39
msgid "Sat."
msgstr ""

#: base/weeks.py:40
msgid "Sun."
msgstr ""

#: configuration/templates/configuration/configuration.html:11
msgid ""
"Step 1 - Database configuration (teachers, groups, rooms, course types, etc.)"
msgstr ""

#: configuration/templates/configuration/configuration.html:12
msgid "Option A: Using a spreadsheet file"
msgstr ""

#: configuration/templates/configuration/configuration.html:14
msgid "Configuration file canvas"
msgstr ""

#: configuration/templates/configuration/configuration.html:15
#: configuration/templates/configuration/configuration.html:50
msgid "Download and then complete the file below."
msgstr ""

#: configuration/templates/configuration/configuration.html:16
msgid "Click to download the configuration file"
msgstr ""

#: configuration/templates/configuration/configuration.html:17
#: configuration/templates/configuration/configuration.html:58
msgid "Download"
msgstr ""

#: configuration/templates/configuration/configuration.html:21
msgid "Configuration file completed"
msgstr ""

#: configuration/templates/configuration/configuration.html:22
msgid "Upload the completed configuration file."
msgstr ""

#: configuration/templates/configuration/configuration.html:23
#: configuration/templates/configuration/configuration.html:65
msgid "Only xlsx format is accepted."
msgstr ""

#: configuration/templates/configuration/configuration.html:27
#: ics/templates/ics/index.html:84
msgid "Department"
msgstr ""

#: configuration/templates/configuration/configuration.html:28
#: people/templates/people/login.html:86
msgid "Create"
msgstr ""

#: configuration/templates/configuration/configuration.html:29
msgid "Edit"
msgstr ""

#: configuration/templates/configuration/configuration.html:32
#: configuration/templates/configuration/configuration.html:76
#: templates/base.html:94
msgid "Import"
msgstr ""

#: configuration/templates/configuration/configuration.html:37
#: configuration/templates/configuration/configuration.html:79
msgid "Verdict :"
msgstr ""

#: configuration/templates/configuration/configuration.html:41
msgid "Option B: With Flopeditor"
msgstr ""

#: configuration/templates/configuration/configuration.html:43
msgid "Click here"
msgstr ""

#: configuration/templates/configuration/configuration.html:43
msgid " to go to Flopeditor."
msgstr ""

#: configuration/templates/configuration/configuration.html:44
msgid "If you need help, you can consult the Flopeditor documentation on "
msgstr ""

#: configuration/templates/configuration/configuration.html:44
msgid "this page."
msgstr ""

#: configuration/templates/configuration/configuration.html:47
msgid ""
"Step 2 - Plan the lessons (which lesson in which week, which teacher gives "
"which lesson)"
msgstr ""

#: configuration/templates/configuration/configuration.html:49
msgid "Template for the planning file from the configuration file"
msgstr ""

#: configuration/templates/configuration/configuration.html:51
msgid "Click to download the planning file outline"
msgstr ""

#: configuration/templates/configuration/configuration.html:63
msgid "Completed planning file"
msgstr ""

#: configuration/templates/configuration/configuration.html:64
msgid "Upload the completed planning file."
msgstr ""

#: configuration/templates/configuration/configuration.html:75
msgid "Stabilization ?"
msgstr ""

#: ics/templates/ics/index.html:35
msgid "iCal Subscription"
msgstr ""

#: ics/templates/ics/index.html:36
msgid ""
"Copy the corresponding link of the calendar you would like to add to your "
"calendar manager."
msgstr ""

#: ics/templates/ics/index.html:38
msgid "Groups"
msgstr ""

#: ics/templates/ics/index.html:41
msgid "Training program"
msgstr ""

#: ics/templates/ics/index.html:41
msgid "Group"
msgstr ""

#: ics/templates/ics/index.html:46 ics/templates/ics/index.html:62
#: ics/templates/ics/index.html:75 ics/templates/ics/index.html:88
msgid "Copy this link"
msgstr ""

#: ics/templates/ics/index.html:52
msgid "Tutors"
msgstr ""

#: ics/templates/ics/index.html:55
msgid "Pseudo"
msgstr ""

#: ics/templates/ics/index.html:55
msgid "First name"
msgstr ""

#: ics/templates/ics/index.html:55
msgid "Last lame"
msgstr ""

#: ics/templates/ics/index.html:55
msgid "Department(s)"
msgstr ""

#: ics/templates/ics/index.html:55 ics/templates/ics/index.html:71
#: ics/templates/ics/index.html:84
msgid "Address"
msgstr ""

#: ics/templates/ics/index.html:71
msgid "Name"
msgstr ""

#: ics/templates/ics/index.html:81
msgid "Planned generations"
msgstr ""

#: people/templates/people/login.html:55
msgid "Forgot password?"
msgstr ""

#: people/templates/people/login.html:58 templates/base.html:108
msgid "Sign in"
msgstr ""

#: people/templates/people/login.html:79
msgid "Sign up"
msgstr ""

#: people/templates/people/login.html:80
msgid "I am"
msgstr ""

#: people/templates/people/login.html:82
msgid "A student"
msgstr ""

#: people/templates/people/login.html:83
msgid "A teacher"
msgstr ""

#: people/templates/people/login.html:84
msgid "A temporary teacher"
msgstr ""

#: people/templates/people/login.html:85
msgid "A tech or administrative staff"
msgstr ""

#: people/templates/people/studentPreferencesSelection.html:34
msgid "Student Preferences"
msgstr ""

#: people/templates/people/studentPreferencesSelection.html:43
msgid "Preferences for your group"
msgstr ""

#: people/templates/people/studentPreferencesSelection.html:45
msgid "Morning or evening?"
msgstr ""

#: people/templates/people/studentPreferencesSelection.html:52
msgid "Busy days VS Free half-days"
msgstr ""

#: people/templates/people/studentPreferencesSelection.html:60
msgid "Holes?"
msgstr ""

#: people/templates/people/studentPreferencesSelection.html:66
msgid "Eat early or late?"
msgstr ""

#: people/views.py:237
msgid "Not allowed"
msgstr ""

#: people/views.py:243
#, python-brace-format
msgid "No such user as {user}"
msgstr ""

#: quote/forms.py:41
msgid "Quote "
msgstr ""

#: quote/forms.py:42
msgid "Last name "
msgstr ""

#: quote/forms.py:43
msgid "First name "
msgstr ""

#: quote/forms.py:44
msgid "Nickname "
msgstr ""

#: quote/forms.py:45
msgid "Author description "
msgstr ""

#: quote/forms.py:46
msgid "Date "
msgstr ""

#: quote/forms.py:47
msgid "Header "
msgstr ""

#: quote/forms.py:48
msgid "Category "
msgstr ""

#: solve_board/templates/solve_board/main-board.html:32
msgid "Schedule Generation"
msgstr ""

#: solve_board/templates/solve_board/main-board.html:60
msgid "Constraints"
msgstr ""

#: solve_board/templates/solve_board/main-board.html:61
msgid ""
"All constraints appearing in bolded - with no weight - will be considered "
"mandatory, the other ones will considered as preferences."
msgstr ""

#: solve_board/templates/solve_board/main-board.html:66
msgid "Configuration"
msgstr ""

#: solve_board/templates/solve_board/main-board.html:70
msgid "Week⋅s"
msgstr ""

#: solve_board/templates/solve_board/main-board.html:74
msgid "Training Program"
msgstr ""

#: solve_board/templates/solve_board/main-board.html:78
msgid "Stabilization"
msgstr ""

#: solve_board/templates/solve_board/main-board.html:83
msgid "Solver"
msgstr ""

#: solve_board/templates/solve_board/main-board.html:94
msgid "Maximum time limit for resolution (minuts)"
msgstr ""

#: solve_board/templates/solve_board/main-board.html:100
msgid "Unlimited"
msgstr ""

#: templates/base.html:53
msgid "Schedule_title"
msgstr ""

#: templates/base.html:66
msgid "<span id=flopGreen >flop</span>!<span id=EDTRed >Scheduler</span>"
msgstr ""

#: templates/base.html:82
msgid "Schedule_banner"
msgstr ""

#: templates/base.html:84
msgid "Move/Cancel"
msgstr ""

#: templates/base.html:86
msgid "Preferences"
msgstr ""

#: templates/base.html:87
msgid "iCal"
msgstr ""

#: templates/base.html:88
msgid "Help"
msgstr ""

#: templates/base.html:89
msgid "Messages"
msgstr ""

#: templates/base.html:90
msgid "Modules"
msgstr ""

#: templates/base.html:92
msgid "Generate"
msgstr ""

#: templates/base.html:93
msgid "Statistics"
msgstr ""

#: templates/base.html:95
msgid "Admin"
msgstr ""

#: templates/base.html:101 templates/base.html:103 templates/base.html:105
msgid "Sign out"
msgstr ""

#: templates/base.html:112
msgid "Signed in as "
msgstr ""
